package com.example.alexander.calculatorextended;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.content.ContentValues;

/**
 * Responsible for Managing the 'calculator.db' Database with SQLite
 * Call and create methods within this to interact with the database
 */

public class DBManager extends SQLiteOpenHelper
{
    //Setting up the Parameters for the Database
    private static final int DATABASE_VERSION = 1;                //Any changes to the Stored values will need to be updated
    private static final String DATABASE_NAME = "calculator.db";  //Name of the Database
    public static final String TABLENAME_SOL = "solutions";       //Name of one of the possible tables within the Database (NOTE: DO NOT use TABLE_NAME as a declaration - SQL hates it)
    public static final String COLUMN1_ID = "_id";                //Column 1 contains the _id  (underscore is requirement)
    public static final String COLUMN2_EQUATION = "equation";     //Column 2 contains the equation+solution

    //Constructor
    public DBManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        //CREATE TABLE items(_id INTEGER PRIMARY KEY AUTOINCREMENT, itemName TEXT);
        String createQuery = "CREATE TABLE " + TABLENAME_SOL  + "(" +
                COLUMN1_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN2_EQUATION + " TEXT " +
                ");";
        db.execSQL(createQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL( "DROP TABLE IF EXISTS " + TABLENAME_SOL  );    //Delete if a Database Exists
        onCreate(db);
    }
}
